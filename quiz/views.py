from collections import defaultdict
from django.shortcuts import get_object_or_404, render, redirect
from .models import Subject, Quiz, Question, Option, UserResult
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from datetime import datetime
from .forms import QuizForm, SubjForm
from django.contrib.auth.decorators import login_required, user_passes_test
import json


# displays 5 newest quizzes
class IndexView(generic.ListView):
    template_name = 'quiz/index.html'
    context_object_name = 'latest_quizzes_list'

    def get_queryset(self):
        return Quiz.objects.order_by('-pub_date')[:5]


# displays a list of all subjects
class SubjectsView(generic.ListView):
    template_name = 'quiz/all_subjects.html'
    context_object_name = 'subjects'

    def get_queryset(self):
        return Subject.objects.all()


# displays a list of all quizzes
class QuizzesView(generic.ListView):
    model = Quiz
    template_name = 'quiz/all_quizzes.html'
    context_object_name = 'quizzes'


# displays all quizzes on given subject
class SubjQuizzesView(generic.ListView):
    template_name = 'quiz/subject_quizzes.html'
    context_object_name = 'quizzes'

    def get_queryset(self):
        subject_id = self.kwargs['pk']
        return Quiz.objects.filter(subject=subject_id)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subject'] = Subject.objects.get(pk=self.kwargs['pk'])
        return context


class QuizDetailView(generic.DetailView):
    model = Quiz
    template_name = 'quiz/detail.html'


class StartQuizView(LoginRequiredMixin, generic.DetailView):
    model = Quiz
    template_name = 'quiz/start_quiz.html'


def submit_quiz(request, quiz_id):
    current_user = request.user
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    try:
        user_answers = {}
        answers_json = defaultdict(dict)
        current_score = 0
        questions = quiz.question_set.all()
        for question in questions:
            if question.question_type == 'RB':
                selected_option = question.option_set.get(pk=request.POST['question{}'.format(question.id)])
                user_answers[question] = selected_option
                answers_json[question.question_text][selected_option.option_text] = selected_option.correct_option
                if selected_option.correct_option:
                    current_score += 1
            elif question.question_type == 'CB':
                selected_options = question.option_set.filter(
                    pk__in=request.POST.getlist('question{}'.format(question.id)))
                correct_options = question.option_set.filter(correct_option=True)
                user_answers[question] = selected_options
                for sel_option in selected_options:
                    answers_json[question.question_text][sel_option.option_text] = sel_option.correct_option
                if len(selected_options) == len(correct_options) and all(
                        [opt.correct_option for opt in selected_options]):
                    current_score += 1
            elif question.question_type == 'TXT':
                selected_option = request.POST['question{}'.format(question.id)]
                correct_options = question.option_set.filter(correct_option=True)
                if selected_option.strip().lower() in map(lambda s: s.option_text.strip().lower(), correct_options):
                    current_score += 1
                    answers_json[question.question_text][selected_option] = True
                    user_answers[question] = [selected_option, True, correct_options[0]]
                else:
                    answers_json[question.question_text][selected_option] = False
                    user_answers[question] = [selected_option, False, correct_options[0]]
        answers_json = json.dumps(answers_json)
        new_result = UserResult(user=current_user,
                                quiz=quiz,
                                score=current_score,
                                date_taken=datetime.now(),
                                answers=answers_json)
        new_result.save()
    except (KeyError, Option.DoesNotExist):
        return render(request, 'quiz/start_quiz.html', {
            'quiz': quiz,
            'error_message': 'You didn\'t select an answer'
        })
    else:
        return render(request, 'quiz/submit_quiz.html', {'result': new_result,
                                                         'user_answers': user_answers,
                                                         'answers_json': answers_json,
                                                         })


def profile(request, user_id=None, username=None):
    if user_id:
        user = get_object_or_404(User, pk=user_id)
    elif username:
        user = get_object_or_404(User, username=username)
    all_user_results = UserResult.objects.filter(user=user)
    if all_user_results:
        latest_user_results = all_user_results.order_by('-date_taken')[:5]
        ordered_by_score_percent = sorted(all_user_results,
                                          key=lambda r: int(r.calc_score_percent()[:-1]),
                                          reverse=True)[:5]
        best_user_results = ordered_by_score_percent
        return render(request, 'quiz/profile.html', {'profile_user': user,
                                                     'has_results': True,
                                                     'latest_user_results': latest_user_results,
                                                     'best_user_results': best_user_results})
    else:
        return render(request, 'quiz/profile.html', {'profile_user': user,
                                                     'has_results': False})


def all_user_results(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    all_user_results = UserResult.objects.filter(user=user)
    return render(request, 'quiz/all_user_results.html', {'profile_user': user,
                                                          'results': all_user_results})


def can_create(user):
    return user.is_superuser


@user_passes_test(can_create)
def new_quiz(request):
    quiz_form = QuizForm()
    return render(request, 'quiz/new_quiz.html', {'quiz_form': quiz_form})


def quiz_created(request):
    if request.method == 'POST':
        hidden_input = request.POST['quiz_json']
        quiz_form = QuizForm(request.POST)
        if quiz_form.is_valid():
            quiz_form.save()
            questions_json = json.loads(hidden_input)
            for question in [questions_json[q] for q in questions_json]:
                if question['question_type'] == 'radioQuestion':
                    quesType = 'RB'
                elif question['question_type'] == 'checkboxQuestion':
                    quesType = 'CB'
                elif question['question_type'] == 'textQuestion':
                    quesType = 'TXT'
                quesText = question['question_text']
                new_ques = Question(quiz=quiz_form.instance,
                                    question_type=quesType,
                                    question_text=quesText)
                new_ques.save()
                options = question['options']
                for option in options:
                    new_option = Option(question=new_ques,
                                        option_text=option,
                                        correct_option=options[option])
                    new_option.save()
        return render(request, 'quiz/quiz_created.html', {'quiz_name': quiz_form.instance.quiz_name})


def delete_quiz(request, quiz_id):
    quiz = get_object_or_404(Quiz, id=quiz_id)
    quiz_name = quiz.quiz_name
    quiz.delete()
    return render(request, 'quiz/delete_quiz.html', {'quiz_name': quiz_name})


@user_passes_test(can_create)
def new_subject(request):
    subj_form = SubjForm()
    return render(request, 'quiz/new_subject.html', {'subj_form': subj_form})


def subject_created(request):
    if request.method == 'POST':
        subj_form = SubjForm(request.POST)
        if subj_form.is_valid():
            subj_name = request.POST['subj_name']
            new_subj = Subject(subj_name=subj_name)
            new_subj.save()
        return render(request, 'quiz/subject_created.html', {'subj_name': subj_name})


def delete_subject(request, subject_id):
    subject = get_object_or_404(Subject, id=subject_id)
    subj_name = subject.subj_name
    subject.delete()
    return render(request, 'quiz/delete_subject.html', {'subj_name': subj_name})


@login_required(login_url='/accounts/login/')
def view_results(request, quiz_id=None, user_id=None, subject_id=None):
    cur_user = request.user
    results_type = 'all'
    results_filter = 'none'
    results_set = UserResult.objects.all()
    if quiz_id:
        results_type = 'by_quiz'
        results_filter = str(Quiz.objects.get(pk=quiz_id))
        results_set = results_set.filter(quiz_id=quiz_id)
    if subject_id:
        results_type = 'by_subj'
        results_filter = str(Subject.objects.get(pk=subject_id))
        results_set = results_set.filter(quiz__subject__pk=subject_id)
    if user_id:
        results_type = 'by_user'
        results_filter = str(User.objects.get(pk=user_id))
        results_set = results_set.filter(user_id=user_id)
    if not cur_user.userrole.isSupervisor and not cur_user.is_superuser:
        results_set = results_set.filter(user_id=cur_user.id)
    all_subjects = Subject.objects.all()
    all_quizzes = Quiz.objects.all()
    all_users = User.objects.all()
    return render(request,
                  'quiz/results.html',
                  {
                      'results_type': results_type,
                      'results_filter': results_filter,
                      'results_set': results_set,
                      'all_subjects': all_subjects,
                      'all_quizzes': all_quizzes,
                      'all_users': all_users,
                  })


@login_required(login_url='/accounts/login/')
def result_details(request, result_id):
    res = get_object_or_404(UserResult, id=result_id)
    return render(request,
                  'quiz/result_details.html',
                  {
                      'answers': res,
                  })


def get_results_table_data(request, quiz_id=None, user_id=None, subject_id=None):
    cur_user = request.user
    results_set = UserResult.objects.all()
    if quiz_id:
        results_set = results_set.filter(quiz_id=quiz_id)
    if subject_id:
        results_set = results_set.filter(quiz__subject__pk=subject_id)
    if user_id:
        results_set = results_set.filter(user_id=user_id)
    if not cur_user.userrole.isSupervisor and not cur_user.is_superuser:
        results_set = results_set.filter(user_id=cur_user.id)
    return render(request,
                  'quiz/get_results_table_data.html',
                  {
                      'results': results_set,
                  })


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('quiz:index')
    else:
        form = UserCreationForm()
    return render(request, 'quiz/signup.html', {'form': form})
