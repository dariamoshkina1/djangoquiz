from django.urls import path, re_path
from django.contrib.auth import views as auth_views

from . import views

app_name = 'quiz'
urlpatterns = [
    # basic pages
    path('', views.IndexView.as_view(), name='index'),
    path('subjects/', views.SubjectsView.as_view(), name='all_subjects'),
    path('quizzes/', views.QuizzesView.as_view(), name='all_quizzes'),
    path('subjects/<int:pk>/', views.SubjQuizzesView.as_view(), name='subj_quizzes'),

    # taking a quiz
    path('subjects/<int:subject_id>/<int:pk>/', views.QuizDetailView.as_view(), name='quiz_detail'),
    path('<int:subject_id>/<int:pk>/start/', views.StartQuizView.as_view(), name='start_quiz'),
    path('<int:quiz_id>/submit/', views.submit_quiz, name='submit'),

    # user profile
    path('profile/<int:user_id>/', views.profile, name='profile'),
    re_path(r'^profile/(?P<username>[\w\d_.@+-]+)/$', views.profile, name='profile'),
    path('profile/<int:user_id>/all_results/', views.all_user_results, name='all_user_results'),

    # editing quizzes
    path('quizzes/create/', views.new_quiz, name='new_quiz'),
    path('quizzes/created', views.quiz_created, name='quiz_created'),
    path('quizzes/<int:quiz_id>/delete/', views.delete_quiz, name='delete_quiz'),

    # editing subjects
    path('subjects/create/', views.new_subject, name='new_subject'),
    path('subjects/created/', views.subject_created, name='subject_created'),
    path('subjects/<int:subject_id>/delete/', views.delete_subject, name='delete_subject'),

    # viewing results
    path('results/', views.view_results, name='all_results'),
    path('results/quizzes/<int:quiz_id>/', views.view_results, name='results_by_quiz'),
    path('results/users/<int:user_id>/', views.view_results, name='results_by_user'),
    path('results/subjects/<int:subject_id>/', views.view_results, name='results_by_subject'),
    path('results/<int:result_id>', views.result_details, name='result_details'),
    path('ajax/results/subjects/<int:subject_id>/', views.get_results_table_data, name='ajax_results_by_subj'),
    path('ajax/results/users/<int:user_id>/', views.get_results_table_data, name='ajax_results_by_user'),
    path('ajax/results/quizzes/<int:quiz_id>/', views.get_results_table_data, name='ajax_results_by_quiz'),
    path('ajax/results/', views.get_results_table_data, name='ajax_all_results'),

    # signup and password reset
    path('signup/', views.signup, name='signup'),
    re_path(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    re_path(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    re_path(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            auth_views.password_reset_confirm, name='password_reset_confirm'),
    re_path(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),

]
