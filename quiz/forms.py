from django import forms
from .models import Quiz, Question, Option
from django.forms.models import BaseInlineFormSet, inlineformset_factory
from datetime import datetime


class QuizForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = ('subject', 'quiz_name', 'difficulty', 'description')
        widgets = {
            'description': forms.Textarea(attrs={'cols': 45, 'rows': 3}),
        }
        labels = {
            'subject': 'Choose quiz subject',
            'quiz_name': 'Enter quiz name',
            'description': 'Enter quiz description',
            'difficulty': 'Choose quiz difficulty'
        }

    def save(self, commit=True):
        quiz = super(QuizForm, self).save(commit=False)
        quiz.pub_date = datetime.now()
        if commit:
            quiz.save()
        return quiz


class SubjForm(forms.Form):
    subj_name = forms.CharField(label='Enter subject name')


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('question_type', 'question_text')
        widgets = {
            'question_text': forms.Textarea(attrs={'cols': 45, 'rows': 3}),
        }
        labels = {
            'question_type': 'Question type',
            'question_text': 'Question text'
        }


OptionFormset = inlineformset_factory(Question,
                                      Option,
                                      exclude=['question'],
                                      widgets={
                                          'explanation': forms.Textarea(attrs={'rows': 3, 'cols': 45})
                                      },
                                      extra=0)


class BaseQuestionsFormset(BaseInlineFormSet):
    def add_fields(self, form, index):
        super(BaseQuestionsFormset, self).add_fields(form, index)
        form.nested = OptionFormset(
                        instance=form.instance,
                        data=form.data if form.is_bound else None,
                        files=form.files if form.is_bound else None,
                        prefix='option-%s-%s' % (
                            form.prefix,
                            OptionFormset.get_default_prefix()))

    def is_valid(self):
        result = super(BaseQuestionsFormset, self).is_valid()
        if self.is_bound:
            for form in self.forms:
                if hasattr(form, 'nested'):
                    result = result and form.nested.is_valid()
        return result

    def save(self, commit=True):
        result = super(BaseQuestionsFormset, self).save(commit=commit)
        for form in self.forms:
            if hasattr(form, 'nested'):
                if not self._should_delete_form(form):
                    form.nested.save(commit=commit)
        return result


QuestionsFormset = inlineformset_factory(Quiz,
                                         Question,
                                         exclude=['quiz'],
                                         formset=BaseQuestionsFormset,
                                         widgets={
                                             'question_text': forms.Textarea(attrs={'rows': 3, 'cols': 45})
                                         },
                                         extra=0)


class BaseQuizFormset(BaseInlineFormSet):
    def add_fields(self, form, index):
        super(BaseQuizFormset, self).add_fields(form, index)
        form.nested = QuestionsFormset(
                        instance=form.instance,
                        data=form.data if form.is_bound else None,
                        files=form.files if form.is_bound else None,
                        prefix='question-%s-%s' % (
                            form.prefix,
                            QuestionsFormset.get_default_prefix()))

    def is_valid(self):
        result = super(BaseQuizFormset, self).is_valid()
        if self.is_bound:
            for form in self.forms:
                if hasattr(form, 'nested'):
                    result = result and form.nested.is_valid()
        return result

    def save(self, commit=True):
        result = super(BaseQuizFormset, self).save(commit=commit)
        for form in self.forms:
            if hasattr(form, 'nested'):
                if not self._should_delete_form(form):
                    form.nested.save(commit=commit)
        return result
