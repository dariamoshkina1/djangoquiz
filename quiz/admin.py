from django.contrib import admin

from .models import Subject, Quiz, Question, Option, UserResult, UserRole
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin

admin.site.register(Subject)

@admin.register(Quiz)
class QuizAdmin(admin.ModelAdmin):
    list_display = ('subject', 'quiz_name', 'difficulty', 'pub_date', 'description')

class OptionInline(admin.TabularInline):
    model = Option
    extra = 3

@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('quiz', 'question_type', 'question_text')
    inlines = [OptionInline]


@admin.register(UserResult)
class UserResultAdmin(admin.ModelAdmin):
    list_display = ('user', 'quiz', 'score', 'date_taken')


class UserRoleInLIne(admin.StackedInline):
    model = UserRole
    can_delete = False


class UserProfileAdmin(UserAdmin):
    inlines = [UserRoleInLIne, ]


admin.site.unregister(User)
admin.site.register(User, UserProfileAdmin)
admin.site.register(UserRole)



