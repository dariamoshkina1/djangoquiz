function filterTable(n, inputName, tableName) {
    var input, filter, table, tr, td, i;
    input = document.getElementById(inputName);
    filter = input.value.toUpperCase();
    if (filter) {
		table = document.getElementById(tableName);
		tr = table.getElementsByTagName("tr");

		for (i=1; i < tr.length; i++) {
			if (n==0) {
				td = tr[i].getElementsByTagName("td")[0];
			}
		    if (n==1){
		    	td = tr[i].getElementsByTagName("td")[1];
		    }
		    if (td) {
		        if (td.textContent.toUpperCase().indexOf(filter)>-1) {
		            tr[i].style.display = "";
		        } else {
		            tr[i].style.display = "none";
		        }
		    }
		}
    }
    else {
    	paginateTableByItemsLimit(tableName);
    }
}

function paginateTableByItemsLimit(tableName){
    var itemsLimit, navbar, pageBars;
    itemsLimit = selectBox.options[selectBox.selectedIndex].value;
    navbar = document.getElementById("nav-wrapper");
    pageBars = document.getElementsByClassName("page-navigation");
    if (document.getElementsByClassName("page-navigation").length>0) {
    	navbar.removeChild(document.getElementsByClassName("page-navigation")[0]);
	};
    $("#"+tableName).paginate({ limit: itemsLimit,
    						optional: false,
                            navigationWrapper: $("#nav-wrapper") });
}