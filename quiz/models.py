from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Subject(models.Model):
    subj_name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.subj_name


class Quiz(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    quiz_name = models.CharField(max_length=200)
    EASY = 'EZ'
    MEDIUM = 'MD'
    HARD = 'HD'
    DIFFICULTY_CHOICES = (
        (EASY, 'Easy'),
        (MEDIUM, 'Medium'),
        (HARD, 'Hard'),
    )
    difficulty = models.CharField(
        max_length=2,
        choices=DIFFICULTY_CHOICES,
        default=EASY,
    )
    pub_date = models.DateTimeField('date published')
    description = models.TextField(default="")

    def __str__(self):
        return self.quiz_name

    def max_result(self):
        return len(self.question_set.all())


class Question(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    RADIO_BUTTON = 'RB'
    CHECK_BOX = 'CB'
    TEXT_FIELD = 'TXT'
    QUESTION_TYPE_CHOICES = (
        (RADIO_BUTTON, 'Radio button'),
        (CHECK_BOX, 'Check box'),
        (TEXT_FIELD, 'Text field')
    )
    question_type = models.CharField(
        max_length=3,
        choices=QUESTION_TYPE_CHOICES,
        default=RADIO_BUTTON
    )
    question_text = models.TextField()

    def __str__(self):
        return self.question_text

    def get_correct_option(self):
        if self.question_type == 'RB':
            return self.option_set.get(correct_option=True)
        elif self.question_type == 'CB':
            return self.option_set.filter(correct_option=True)


class Option(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    option_text = models.CharField(max_length=200)
    correct_option = models.BooleanField(default=False)
    explanation = models.TextField(default="", blank=True)

    def __str__(self):
        return "{} ({})".format(self.option_text, str(self.correct_option))


class UserResult(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    score = models.IntegerField(default=0)
    date_taken = models.DateTimeField(blank=True)
    answers = models.TextField(default='')
    def calc_score_percent(self):
        q = self.quiz
        max_res = q.max_result()
        return '{}%'.format(int(round(self.score/max_res, 2)*100))


class UserRole(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True
    )
    isStudent = models.BooleanField(default=True)
    isSupervisor = models.BooleanField(default=False)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserRole.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.userrole.save()

    def __str__(self):
        return '{}: student {}, supervisor {}, superuser {}'.format(self.user.username,
                                                                    self.isStudent,
                                                                    self.isSupervisor,
                                                                    self.user.is_superuser)